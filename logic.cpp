#include "logic.h"
#include "const.h"
#include "animal.h"
#include "loup.h"
#include "ours.h"
#include "lion.h"
#include "pierre.h"
using namespace std;
#include <string>
#include <iostream>

void init(std::vector<animal*> *joueurs, int nbJoueurs, animal* plateau[][MAX_Y]) {
	for(int i = 0; i < MAX_X; i++) {
		for(int j = 0; j < MAX_Y; j++) {
			plateau[i][j] = nullptr;
		}
	}
	for(int i = 0; i<nbJoueurs; i++) {
		int type = rand() % 4;
		int suicide = rand() % 10;
		int x = -1;
		int y = -1;
		do {
			x = rand() % MAX_X;
			y = rand() % MAX_Y;
		} while(plateau[x][y] != nullptr);
		//std::cout<<type<<std::endl;
		switch(type) {
			case 0: { //lion
			lion* nvAnimal = new lion(suicide, x, y);
			plateau[x][y] = nvAnimal;
			joueurs->push_back(nvAnimal);
			break; }
			case 1: {//ours
			ours* nvAnimal = new ours(suicide, x, y);
			plateau[x][y] = nvAnimal;
			joueurs->push_back(nvAnimal);
			break; }
			case 2: { //pierre
			pierre* nvAnimal = new pierre(0, x, y);
			plateau[x][y] = nvAnimal;
			joueurs->push_back(nvAnimal);
			break; }
			case 3: { //loup
			loup* nvAnimal = new loup(suicide, x, y);
			plateau[x][y] = nvAnimal;
			joueurs->push_back(nvAnimal);
			break; }
		}


	}

	//std::cout<<joueurs->size()<<std::endl;
}

void update(std::vector<animal*> joueurs, animal* plateau[][MAX_Y]) {
	for(int i=0; i<joueurs.size(); ++i)
	{
		//cout<<joueurs.size()<<endl;
		if(joueurs[i]->getAlive()) {
			int oldX = joueurs[i]->getX();
			int oldY = joueurs[i]->getY();
			if(joueurs[i]->deplacement(plateau)) {
				plateau[oldX][oldY] = nullptr;
				if(plateau[joueurs[i]->getX()][joueurs[i]->getY()] != 0) { //S'il y a déjà un animal à cette case
					if(joueurs[i]->attaquer(plateau[joueurs[i]->getX()][joueurs[i]->getY()])) { //Si l'attaque succède

						cout<<"Un "<< joueurs[i]->getNom() << " tue un " << plateau[joueurs[i]->getX()][joueurs[i]->getY()]->getNom()<<endl;
						plateau[joueurs[i]->getX()][joueurs[i]->getY()]->mourir();
						plateau[joueurs[i]->getX()][joueurs[i]->getY()] = joueurs[i];
					} else {	//Si l'attaque est perdue

						cout<<"Un "<< plateau[joueurs[i]->getX()][joueurs[i]->getY()]->getNom() << " tue un " << joueurs[i]->getNom()<<endl;
						joueurs[i]->mourir();
					}
				}else {
					plateau[joueurs[i]->getX()][joueurs[i]->getY()] = joueurs[i];
				}
			}
		}
	}
}

void display(animal* plateau[][MAX_Y]) {
	std::string texte;
	int nbretype[4] = {0};
	for(int i = 0; i < MAX_X; i++) {
		for(int j = 0; j < MAX_Y; j++) {
			if(plateau[i][j] != nullptr) {
				switch(plateau[i][j]->getType()) {
					case 0:
					nbretype[0]++;
					texte = "L";
					break;
					case 1:
					nbretype[1]++;
					texte = "O";
					break;
					case 2:
					nbretype[2]++;
					texte = "P";
					break;
					case 3:
					nbretype[3]++;
					texte = "W";
					break;
				}
			} else {
				texte = "_";
			}
			std::cout << texte;
		}
		std::cout << std::endl<<std::endl;
	}
	std::cout<< "Il reste " << nbretype[0] << " lions, " << nbretype[1] << " ours, " << nbretype[2] << " pierres, " << nbretype[3] << " loups";
	std::cout << std::endl<< std::endl<< std::endl<< std::endl;
}
