#pragma once
#include "animal.h"
#include "const.h"

class pierre : public animal {
    public :
		pierre(int _p, int _x, int _y);
		int generateAtt( animal* adversaire);
		bool deplacement(animal* tab[][MAX_Y]);

		~pierre() {}
};
