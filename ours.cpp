#include "ours.h"
#include "const.h"

ours::ours(int _p, int _x, int _y) : animal(p , _x, _y) {
	temp_dep = {3,3,3,3,2,2,2,2,1,1,1,1,4,4,4,4};
	dep = 0;
	type = 1;
	nom = "ours";
}

bool ours::deplacement(animal* tab[][MAX_Y]) {

	int depl = temp_dep[dep];
	dep ++;
	if ( temp_dep.size() == dep )
		dep = 0;

    deplacer(depl);
    return true;
}

int ours::generateAtt( animal* adversaire) {
	return 1;
}
