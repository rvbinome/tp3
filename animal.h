#pragma once
#include <vector>
#include <random>
#include "const.h"
class animal {
	private :

	protected :
		std::vector<int> temp_dep;
		unsigned int dep;
		int type; // 0 lion, 1 ours, 2 pierre, 3 loup
		std::string nom;
		void deplacer ( int dir );
		int p;
		int x,y;
		bool combat( int a, int b);
		bool alive;
	public :

		animal(int _p,  int _x, int _y);

		int virtual generateAtt( animal* adversaire )=0; // 0 pierre, 1 feuille, 2 ciseaux
		bool virtual deplacement( animal* tab[][MAX_Y])=0;

		bool attaquer(animal* adversaire);// Appeler generateatt de this et adversaire
		bool suicide();
		bool getAlive() { return alive; }
		int getType() { return type;}
		std::string getNom() { return nom;}
		int getX() { return x; }
		int getY() { return y; }

		void mourir() { alive = false; }
		~animal() {}


};
