#include "animal.h"
#include "const.h"

void animal::deplacer( int dir ) {
	// 1 vers le haut, 2 vers la droite, 3 vers le bas, 4 vers la gauche
	if ( dir ==  1) {
		if ( x == 0) {
			x = MAX_X-1;
		}else {
			x -= 1;
		}
	}
	else if ( dir ==  4) {
		if ( y == 0) {
			y = MAX_Y-1;
		}else {
			y -= 1;
		}
	}
	else if ( dir ==  2) {
		if ( y == MAX_Y-1) {
			y = 0;
		}else {
			y += 1;
		}
	}
	else if ( dir ==  3) {
		if ( x == MAX_X-1) {
			x = 0;
		}else {
			x += 1;
		}
	}
}

animal::animal ( int _p,  int _x, int _y) : p(_p), x(_x), y(_y), alive(true) {
}

bool animal::suicide() {
	int randint = rand() % 101;
	if ( randint <= p ) {
		return true;
	}else {
		return false;
	}
}

bool animal::attaquer( animal* adversaire ) {
	if ( suicide()) {
		return false;
	}
	int monAtt = generateAtt( adversaire);
	int advAtt = adversaire->generateAtt( this );
    return combat (monAtt, advAtt);
}

bool animal::combat(int a, int b) {
	// 0 est pierre, 1 est feuille, 2 est ciseau

	if ( a == b ){ // Si match nul, chance de 1/2 de gagner
		return ( rand() % 101 < 50);
	}

	bool retour;
	if ( a == 0 ) {
		if ( b == 1 ) {
			retour = false;
		}else {
			retour = true;
		}
	}
	else if ( a == 1) {
		if ( b == 0 ) {
			retour = true;
		}else {
			retour = false;
		}
	}
	else if ( a == 2) {
		if ( b == 0 ) {
			retour = false;
		}else {
			retour = true;
		}
	}

	return retour;

}
