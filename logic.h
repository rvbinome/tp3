#pragma once
#include "animal.h"
#include "const.h"
#include <vector>
void init(std::vector<animal*> *joueurs, int nbJoueurs, animal* plateau[][MAX_Y]);
void update(std::vector<animal*> joueurs, animal* plateau[][MAX_Y]);
void display(animal* plateau[][MAX_Y]);
