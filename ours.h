#pragma once
#include "animal.h"
#include "const.h"

class ours : public animal {
    public :
		ours(int _p, int _x, int _y);
		int generateAtt( animal* adversaire);
		bool deplacement(animal* tab[][MAX_Y]);

		~ours() {}
};
