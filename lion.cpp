#include "lion.h"
#include "const.h"

lion::lion(int _p, int _x, int _y) : animal(_p , _x, _y) {
	temp_dep = {3, 2};
	dep = 0;
	type = 0;
	nom = "lion";
}

bool lion::deplacement(animal* tab[][MAX_Y]) {

	int depl = temp_dep[dep];
	dep ++;
	if ( temp_dep.size() == dep )
		dep = 0;
    deplacer(depl);
    return true;
}

int lion::generateAtt( animal* adversaire) {

	int alea = rand() % 101;
	int att;
	if ( alea <= 50) {
		att = 1;
	}else {
		att = 2;
	}

	return att;
}
