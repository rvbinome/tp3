#include "loup.h"
#include "const.h"
#include <iostream>
loup::loup(int  _p, int _x, int _y) : animal(p , _x, _y) {
	type = 3;
	nom = "loup";
}

bool loup::deplacement(animal* tab[][MAX_Y]) {

	int depl = 0;
	int i =1;
	while ( i<MAX_X and depl == 0 ) {
		depl = detecterproche( tab, i);
		i++;
	}
	if ( depl != 0) {
		deplacer(depl);
		return true;
	}else {
		return false;
	}
}

int loup::detecterproche( animal* tab[][MAX_Y], int i) {
	int ciblex= -1;
	int cibley= -1;
	//Regarde si dans les bords du carré de sommet ( (x-i,y-i), ( x-i,y+i) , ( x+i, y+i) , (x+i, y-i) )  il y a un adversaire, et on récupère sa position si oui

	for ( int u = x-i; u <= x+i; u++) {
		if ( u >= 0 and u < MAX_X and y-i>=0 ) {//Parcours de la colonne y = y-i
			if ( tab[u][y-i] != nullptr) {
				ciblex = u;
				cibley = y-i;
                //std::cout<<"Hello : "<<ciblex<<" yow : "<<cibley<<std::endl;
			}
		}
		if ( u > 0 and u < MAX_X-1 and y+i < MAX_Y ) {//Parcours de la colonne y = y+i
			if ( tab[u][y+i] != nullptr) {
				ciblex = u;
				cibley = y+i;
               // std::cout<<"Hello : "<<ciblex<<" yow : "<<cibley<<std::endl;
			}
		}
	}
	for ( int u = y-i; u <= y+i; u++) {
		if ( u > 0 and u < MAX_Y-1 and x-i>= 0 ) {//Parcours de la ligne x = x- i
			if ( tab[x-i][u] != nullptr) {
				ciblex = x-i;
				cibley = u;
                //std::cout<<"Hello : "<<ciblex<<" yow : "<<cibley<<std::endl;
			}
		}
		if ( u > 0 and u < MAX_Y-1 and x+i>= 0 ) {//Parcours de la ligne x = x+ i
			if ( tab[x+i][u] != nullptr) {
				ciblex = x+i;
				cibley = u;
               // std::cout<<"Hello : "<<ciblex<<" yow : "<<cibley<<std::endl;
			}
		}
	}


	//std::cout<< ciblex << " " << cibley<<std::endl;

	if ( ciblex != -1 and cibley != -1 ) {
		//On calcule maintenant la meilleure direction pour l'atteindre
		int deltaX = ciblex -x;
		int deltaY = cibley -y;
		int direction= 0;
		//std::cout<< deltaX << " " << deltaY<<std::endl;
		if ( abs(deltaX) > abs(deltaY) ) {
			if ( deltaX > 0 ) {
				direction = 3; // Il faut se déplacer vers le haut
			}else {
				direction = 1; // il faut se déplacer vers le bas
			}
		}else {
			if ( deltaY > 0) {
				direction = 2; // Il faut se déplacer vers la droite
			} else {
				direction = 4; // Il faut se déplacer vers la gauche
			}
		}

		//std::cout<<direction<<std::endl;
		return direction;
	} else {
		return 0;
	}



}

int loup::generateAtt( animal* adversaire) {
	int att=0;
	if ( adversaire->getType() == 0) { // on est contre un lion, on fait soit ciseau soit pierre
		int alea = rand() % 101;
		if ( alea <= 50) {
			att = 0;
		}else {
			att = 2;
		}
	}
	else if ( adversaire->getType() == 1) { // on est contre un ours, on fait que ciseau
		att = 2;
	}
	else if ( adversaire->getType() == 2) { // on est contre une pierre, on fait que feuille
       // std::cout<<"yoooooooooo";
		att = 1;
	}
	else if ( adversaire->getType() == 3) { // on est contre un loup, on fait une attaque aléatoire
		int alea = rand() % 101;
		if ( alea <= 33) {
			att = 0;
		}
		else if ( alea <= 66 ) {
			att = 1;
		}
		else {
			att = 2;
		}
	}

	return att;
}

