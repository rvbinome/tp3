#pragma once
#include "animal.h"
#include "const.h"

class loup : public animal {
    public :
		loup(int  _p, int _x, int _y);
		int generateAtt( animal* adversaire);
		bool deplacement(animal* tab[][MAX_Y]);
		int detecterproche ( animal* tab[][MAX_Y], int i);

		~loup() {}
};
