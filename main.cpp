#include <vector>
#include <time.h>
#include <random>
#include <iostream>
#include <string>
using namespace std;
#include "const.h"
#include "animal.h"
#include "lion.h"
#include "loup.h"
#include "ours.h"
#include "pierre.h"
#include "logic.h"

int main() {
	srand(time(NULL));
	vector<animal*> joueurs;
	animal* plateau[MAX_X][MAX_Y];
	int nbJoueurs = MAX_X*MAX_Y*0.25;
	init(&joueurs, nbJoueurs, plateau);
	display(plateau);
	//cout<<joueurs.size()<<endl;
	int nombre=1;
	while ( nombre != 0 ) {
        cout<<"Choisir le nombre d'it�ration : ( 0 pour arr�ter )"<<endl;
        cin>>nombre;
        for(int i=0; i<nombre; ++i)
        {
            cout<<"It�ration num�ro :  "<<i << " -> "<<endl<<endl<<endl;
            update(joueurs, plateau);

        }
         display(plateau);

	}

	/*for(int i=0; i<joueurs.size(); ++i)
	{
		cout<< "Je suis un "<< joueurs[i]->getNom() << " et je suis " << ( joueurs[i]->getAlive() ? "vivant" : "mort" )<<endl;;
	}*/
}
