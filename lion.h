#include "animal.h"
#include "const.h"
class lion : public animal {
    public :
		lion(int _p, int _x, int _y);
		int generateAtt( animal* adversaire);
		bool deplacement(animal* tab[][MAX_Y]);

		~lion() {}
};
